from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
from matplotlib import pyplot as plt
from mesh import *
from model import *
from optimizer import OptimizerDdiff0, OptimizerLinear
from basis import GaussianBasis
from approximator import LinearApproximator

def create_coefficients(M,
                        D_diff_0,
                        delta_D_diff):
    '''Create random initial velocity and Fourier coefficients for diffusion.

    :arg M: Number of Fourier modes
    :arg D_diff_0: mean diffusion coefficient
    :arg delta_D_diff: maximal variation in diffusion coefficient
    '''
    
    D_diff = np.zeros(2*M+1)
    D_diff[0] = D_diff_0 + 0.5*delta_D_diff*np.random.uniform(-1.,+1.)
    D_diff[1:2*M+1] = delta_D_diff*np.random.uniform(-1.,+1.,size=2*M)
    for k in range(1,M+1):
        D_diff[2*k] *= 0.5**(k+1)
        D_diff[2*k-1] *= 0.5**(k+1)
    return D_diff

def plot_solution(model, gaussian_model, sigma0, t, x_meas):
    '''Calculate and plot solution from full model and Gaussian-plume model
    at time t, assuming the initial condition is a Gaussian with width sigma0.

    In addition, plot the diffusion coefficient. On the plots, show the
    location of the met- and measurement- stations.
    '''
    D_diff_0 = gaussian_model.D_diff_0
    delta_D_diff = model.D_diff_max-gaussian_model.D_diff_0
    plt.clf()
    X = (np.arange(0,mesh.n)+0.5)*mesh.h-0.5*mesh.L
    Y = model.D(X)
    # Plot diffusion coefficient and wind-vector
    plt.subplot(2,1,1)
    plt.plot(X,Y,linewidth=2,color='blue')
    plt.plot(X,0*X+D_diff_0,linewidth=2,color='black')
    plt.plot(X,0*X+D_diff_0-delta_D_diff,
             linewidth=2,color='black',linestyle='--')
    plt.plot(X,0*X+D_diff_0+delta_D_diff,
             linewidth=2,color='black',linestyle='--')
    plt.arrow(0,D_diff_0,gaussian_model.c_adv,0,width=0.1,head_width=0.2,head_length=1.0,color='red')

    ax = plt.gca()
    ax.set_ylim(0,D_diff_0+2*delta_D_diff)
    ax.set_xlim(-0.5*mesh.L,0.5*mesh.L)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'diffusion coefficient $D(x)$')
    plt.subplot(2,1,2)

    # Solve model
    model.solve(sigma0,t)
    gaussian_model.solve(sigma0,t)

    # Plot solution
    Y_full = np.vectorize(model.evaluate_solution)(X)
    Y_gaussian = np.vectorize(gaussian_model.evaluate_solution)(X)
    Y_initial = 1./np.sqrt(2.*np.pi*sigma0**2)*np.exp(-0.5*X**2/sigma0**2)
    plt.plot(X,Y_full,linewidth=2,color='blue',label='full model')
    plt.plot(X,Y_gaussian,linewidth=2,color='red',label='Gaussian model')
    plt.plot(X,Y_initial,linewidth=2,linestyle='--',color='blue',label='initial condition')
    plt.plot(X,1.*(Y_full-Y_gaussian),linewidth=2,color='green',label=r'$1\times$ difference')
    plt.plot(np.asarray(x_meas),0.*np.asarray(x_meas),linewidth=0,markersize=8,marker='s',markeredgecolor='black',markerfacecolor='white',markeredgewidth=2)
    plt.plot(X,0.*X,linewidth=1,color='gray',linestyle='--')
    ax = plt.gca()
    ax.set_xlim(-0.5*mesh.L,0.5*mesh.L)
    y0 = 1.2/np.sqrt(2.*np.pi*(sigma0**2+2.*gaussian_model.D_diff_0*t))
    ax.set_ylim(-y0,y0)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'solution $u(x)$')
    plt.legend(loc='lower right',ncol=2)
    plt.savefig('diffusion.pdf',bbox_inches='tight')

def generate_data(mesh,
                  x_meas,
                  M,
                  delta_c_adv,
                  D_diff_0,
                  delta_D_diff,
                  sigma0,t,
                  n_data):
    ''' Create two arrays, by sampling from artificial met-fields. Let i be
    the i-th generated artificial met-field then we have:

    * met_data[i] = c_adv 
    * meas_data[i,k] = u(x_meas_k), i.e. the exact solution at the k-th
        measurement location

    :arg mesh: computational mesh
    :arg x_meas: list of measurement-station locations
    :arg M: Number of Fourier-coefficients used for generating met-fields
    :arg delta_c_adv: range for velocity for generating met-fields
    :arg D_diff_0: mean diffusion coefficient for generating met-fields
    :arg delta_D_diff: variation in diffusion coefficient for generating
        met-fields
    :arg sigma0: width of initial condition
    :arg t: integration time
    '''
    
    met_data = np.zeros(n_data)
    meas_data = np.zeros((n_data,len(x_meas)))
    for i in range(n_data):
        c_adv = np.random.uniform(-delta_c_adv,+delta_c_adv)
        model = AdvectionDiffusionModel(mesh,
                                        c_adv,
                                        D_diff,
                                        D_diff_0+delta_D_diff)
        met_data[i] = c_adv
        model.solve(sigma0,t,c_adv)
        for j,x in enumerate(x_meas):
            meas_data[i,j] = model.evaluate_solution(x)
    return met_data, meas_data

def plot_data(met_data,meas_data):
    '''Plot measurements of concentration vs. advection velocity for
    all sampling locations.
    
    :arg met_data: Measured advection velocities, array of size n_data
    :arg meas_data: Measured concentrations at sampling stations, array of size
       n_data x n_meas
    '''
    n_data, n_meas = meas_data.shape
    plt.clf()
    ax = plt.gca()
    ax.set_xlabel(r'advection velocity $c_{\operatorname{adv}}$')
    ax.set_ylabel(r'measured concentration $u$')
    for k in range(n_meas):
        c_col = k/float(n_meas)
        X, Y = (list(x) for x in zip(*sorted(zip(met_data[:],
                                                 meas_data[:,k]),
                                             key = lambda pair: pair[0])))
        plt.plot(X,Y,
                 linewidth=0,
                 marker='o',
                 markersize='2',
                 color=(c_col,0,1.-c_col))
    plt.savefig('measurements.pdf',bbox_inches='tight')


def plot_evaluation(x_meas,
                    sigma0,t,delta_c_adv,
                    met_data_train,meas_data_train,
                    met_data_test, meas_data_test,
                    approximator):
    D_diff_0 = approximator.D_diff_0
    
    # Evaluate performance 
    plt.clf()
    for k in range(n_meas):
        plt.subplot(2,3,k+1)
        ax = plt.gca()
        ax.set_ylim(-0.05,0.05)
        if (k==3):
            ax.set_xlabel(r'velocity $c_{\operatorname{adv}}$')
            ax.set_ylabel(r'concentration $u$')
        else:
            ax.set_xticks([])
            ax.set_yticks([])
        # Training data (blue)
        X, Y = (list(x) for x in zip(*sorted(zip(met_data_train[:],meas_data_train[:,k]), key = lambda pair: pair[0])))
        for j in range(len(X)):
            c_adv = X[j]
            model = GaussianPlumeModel(c_adv, D_diff_0)
            model.solve(sigma0, t)
            Y[j] -= model.evaluate_solution(x_meas[k])

        plt.plot(X,Y,
                 linewidth=0,
                 marker='s',
                 markersize=2,
                 color='blue')
        # Test data (red)
        X, Y = (list(x) for x in zip(*sorted(zip(met_data_test[:],meas_data_test[:,k]), key = lambda pair: pair[0])))
        for j in range(len(X)):
            c_adv = X[j]
            model = GaussianPlumeModel(c_adv, D_diff_0)
            model.solve(sigma0, t)
            Y[j] -= model.evaluate_solution(x_meas[k])

        plt.plot(X,Y,
                 linewidth=0,
                 marker='o',
                 markersize='4',
                 color='red')
        # Predicted solution
        X = np.arange(-delta_c_adv,+delta_c_adv,0.001)
        f = lambda x: approximator.evaluate(k,x,True)
        Y = np.vectorize(f)(X)

        plt.plot(X,Y,
                 linewidth=2,
                 color='green')

    plt.savefig('evaluation.pdf',bbox_inches='tight')
    
if (__name__ == '__main__'):
    '''
    ========================================================================
    = M A I N     P R O G R A M
    ========================================================================
    '''
    
    # Initialise random number generator
    np.random.seed(3124277)
    
    # Width of function given as initial condition
    sigma0 = 0.5

    # Final time
    t = 1.0

    # Construct mesh of size L and with n cells. The computational
    # domain is [-L/2,+L/2]
    L = 16.0
    n = 128
    mesh = Mesh(L,n)

    # === Set model parameters ===
    # Number of Fourier modes
    M = 8
    # range for c_avg, which varies in [-delta_c_adv, +delta_c_adv]
    delta_c_adv = 6.0
    # average diffusion coefficient
    D_diff_0 = 4.0
    # range variation of diffusion coefficient, which can be in
    # [D_diff_0-delta_D_diff, D_diff_0+delta_D_diff]
    delta_D_diff = 3.75

    # Create diffusion coefficients 
    D_diff = create_coefficients(M,
                                 D_diff_0,
                                 delta_D_diff)
    D_diff_0 = D_diff[0]

    c_adv = 0.5*delta_c_adv
    
    model = AdvectionDiffusionModel(mesh,
                                    c_adv,
                                    D_diff,
                                    D_diff_0+delta_D_diff)

    gaussian_model = GaussianPlumeModel(c_adv, D_diff_0)
    
    # Generate data
    n_data = 512 # Total number of data points
    n_test = 64 # Number of test samples
    n_train = n_data-n_test # Number of training samples (rest is test data)
    n_meas = 6  # Number of measuring stations
    print ('*** data parameters ***')
    print ('Number of training samples     = ',('%6d' % n_train))
    print ('Number of test samples         = ',('%6d' % n_test))
    print ('Number of measurement stations = ',('%6d' % n_meas))
    x_meas = mesh.L*(1./n_meas*(np.arange(n_meas)+0.5)-0.5)
    met_data, meas_data = generate_data(mesh,
                                        x_meas,
                                        M,
                                        delta_c_adv,
                                        D_diff_0,
                                        delta_D_diff,
                                        sigma0,t,
                                        n_data)

    # extract training data, add random noise
    met_data_train = met_data[:n_train]
    met_data_train += np.random.normal(scale=0.00,size=met_data_train.shape)
    meas_data_train = meas_data[:n_train,:]
    meas_data_train += np.random.normal(scale=0.005,size=meas_data_train.shape)
    # the rest is test-data
    met_data_test = met_data[n_train:]
    meas_data_test = meas_data[n_train:,:]
    
    # Plot solution, meteorology and measurement locations
    plot_solution(model, gaussian_model, sigma0, t, x_meas)

    plot_data(met_data_train,meas_data_train)

    alpha = 0.1 # Learning rate
    batch_size = 16 # Batch size for gradient descent
    maxiter = 1024 # Number of iterations for gradient descent
    n_basis = 16 # Number of basis functions for linear approximation
    print ()
    print ('*** Stochastic Gradient Descent parameters ***')
    print ('Learning rate = ',('%6.2f' % alpha))
    print ('batch size    = ',('%6d' % batch_size))
    print ('maxiter       = ',('%6d' % maxiter))
    print ()
    print ('number of basis functions = ',('%6d' % n_basis))
    approximator = LinearApproximator(sigma0,t,x_meas,delta_c_adv,
                                      alpha=alpha,
                                      batch_size=batch_size,
                                      maxiter=maxiter,
                                      n_basis=n_basis)
    
    approximator.run(met_data_train,meas_data_train)

    plot_evaluation(x_meas,
                    sigma0,t,delta_c_adv,
                    met_data_train,meas_data_train,
                    met_data_test, meas_data_test,
                    approximator)
