import numpy as np

from mesh import *

class GaussianPlumeModel(object):
    ''' Gaussian plume model for advection-diffusion equation
    with constant coefficients D_0 and c

    du/dt = D_0*d^2u/dx^2 - c*du/dx 

    :arg c_adv: advection velocity c
    :arg D_diff_0: diffusion coefficient D_0
    '''
    def __init__(self, c_adv, D_diff_0):
        self.c_adv = c_adv
        self.D_diff_0 = D_diff_0

    ''' Create solution u(x,t) for initial condition which is a Gaussian of 
    width sigma_0 located at position x_0=0    
    '''
    def solve(self, sigma_0, t):
        '''Solve the discretised problem for a given initial solution, which
        is assumed to be a Gaussian of width sigma_0 located at position x_0=0.
        This method create a function which can evaluated with the solve()
        method.

        :arg sigma_0: width of initial Gaussian
        :arg t: time for which the solution should be calculated
        '''
        x_0 = 0.0
        sigma2 = sigma_0**2+2*self.D_diff_0*t
        self.U = lambda x: 1./np.sqrt(2.*np.pi*sigma2)*np.exp(-0.5*(x-x_0-self.c_adv*t)**2/sigma2)
        self.gradDU = lambda x: -t/np.sqrt(2.*np.pi*sigma2**3)*np.exp(-0.5*(x-x_0-self.c_adv*t)**2/sigma2)*(1.-(x-x_0-self.c_adv*t)**2/sigma2)

    ''' Evaluate solution u(x,t) at position x
    '''
    def evaluate_solution(self, x):
        '''Return the value of the solution u(x,t) at a particular position.
        This assumes that the solve() method has been called to create the
        function self.U

        :arg x: position at which to evaluate the solution
        '''
        return self.U(x)

    def evaluate_grad_D0(self, x):
        '''Return the value of the gradient of u(x,t) with respect to
        D_0 at a particular position x.
        This assumes that the solve() method has been called to create the
        function self.U

        :arg x: position at which to evaluate the gradient
        '''
        return self.gradDU(x)

class AdvectionDiffusionModel(object):

    ''' Advection diffusion model for equation

    du/dt = d/dx (D(x) du/dx) - c*du/dx

    where D(x) = D_0 + sum_{k=1}^m ( A_k*cos(2*pi/L*k) + B_k*sin(2*pi/L*k))

    with

    * D_0 = D_diff[0] 
    * A_k = D_diff[2*k]   for k = 1,...,m
    * B_k = D_diff[2*k-1] for k = 1,...,m

    :arg mesh: computational mesh
    :arg c_adv: advection velocity c
    :arg D_diff: vector of length 2*m+1 Fourier-coefficients of diffusion
    :arg D_diff_max: maximal diffusion coefficient (this is needed to set
      a stable time step in the forward-Euler integrator).
    
    '''
    def __init__(self, mesh, c_adv, D_diff, D_diff_max):
        self.mesh = mesh
        self.c_adv = c_adv
        self.D_diff = D_diff
        self.D_diff_max = D_diff_max

    '''Return diffusion coefficient at position x

    :arg x: Position at which to evaluate the diffusion coefficient
    '''
    def D(self,x):
        n_diff = int((len(self.D_diff)-1)/2)
        tmp_D = self.D_diff[0]
        alpha = 2.*np.pi/self.mesh.L*x
        for k in range(1,n_diff+1):            
            tmp_D += self.D_diff[2*k]*np.cos(2.*k*alpha)
            tmp_D += self.D_diff[2*k-1]*np.sin((2.*k-1)*alpha)
        return tmp_D


    def solve(self, sigma_0, t, c_adv=None):
        '''Solve the discretised problem using a forward-Euler integrator
        for a given initial solution, which is assumed to be a Gaussian
        of width sigma_0 located at position x_0. This method populates the
        array self.U, which can then be used to obtain a solution at a
        particular point using the evaluate_solution() method.

        :arg sigma_0: width of initial Gaussian
        :arg t: time for which the solution should be calculated
        :arg c_adv: Advective wind speed (take self.c_adv if None)
        '''
        if (c_adv is None):
            c_adv_tmp = self.c_adv
        else:
            c_adv_tmp = c_adv
        x_0 = 0.0
        # Initialise solution to Gaussian of width sigma_0, centred at x_0
        X = (np.arange(0,self.mesh.n)+0.5)*self.mesh.h-0.5*self.mesh.L
        self.U = 1./np.sqrt(2.*np.pi*sigma_0**2)*np.exp(-0.5*(X-x_0)**2/sigma_0**2)

        # Work out number of time steps
        n_step = int(max(2*self.D_diff_max*t/self.mesh.h**2,
                         2*abs(c_adv_tmp)*t/self.mesh.h)+1)
        dt = t/n_step
        
        A_p = np.zeros(self.mesh.n)
        A_m = np.zeros(self.mesh.n)
        A_0 = np.zeros(self.mesh.n)

        for i in range(self.mesh.n):
            A_p[i] = dt/self.mesh.h**2*self.D(X[i]+0.5*self.mesh.h)
            A_m[i] = dt/self.mesh.h**2*self.D(X[i]-0.5*self.mesh.h)
        if (c_adv_tmp > 0):
            A_m[:] += c_adv_tmp*dt/self.mesh.h            
        if (c_adv_tmp < 0):
            A_p[:] -= c_adv_tmp*dt/self.mesh.h
        A_0[:] = -(A_p[:]+A_m[:])

        for k in range(n_step):
            self.U += np.multiply(A_0,self.U) + np.multiply(A_p,np.roll(self.U,-1)) + np.multiply(A_m,np.roll(self.U,1))

    def evaluate_solution(self,x):
        '''Return the value of the solution u(x,t) at a particular position.
        This assumes that the solve() method has been called to populate
        the array self.U.

        :arg x: position at which to evaluate the solution
        '''
        i = int((x+0.5*self.mesh.L)/self.mesh.h)
        return self.U[i]
