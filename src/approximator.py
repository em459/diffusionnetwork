import numpy as np
from basis import GaussianBasis
from optimizer import OptimizerDdiff0, OptimizerLinear
from model import GaussianPlumeModel

class LinearApproximator(object):
    def __init__(self,sigma0,t,x_meas,
                 delta_c_adv,
                 alpha=0.1,
                 batch_size=16,
                 maxiter=1024,
                 n_basis=16):
        '''Function approximator, based on the sum of a Gaussian and a set
        of Gaussian basis functions

        :arg sigma0: Width of initial solution
        :arg t: time of solution
        :arg x_meas: locations of measurement station
        :arg delta_c_adv: maximal absolute value of advective velocity
        :arg alpha: Gradient descent training rate
        :arg batch_size: Gradient descent batch size
        :arg maxiter: Number of gradient descent iterations
        :arg n_basis: Number of Gaussian basis functions
        '''
        self.sigma0 = sigma0
        self.t = t
        self.x_meas = x_meas
        self.delta_c_adv = delta_c_adv
        self.alpha = alpha
        self.batch_size = batch_size
        self.maxiter = maxiter
        self.n_basis = n_basis

        # Gaussian basis functions
        sigma_basis = 2.*self.delta_c_adv/self.n_basis
        self.basis = GaussianBasis(self.n_basis,
                                   -1.5*self.delta_c_adv,
                                   +1.5*self.delta_c_adv,
                                   sigma_basis)

    def run(self, met_data, meas_data):
        '''Fit data

        :arg met_data: Measured velocities
        :arg meas_data: Measured concentrations at sampling stations
        '''
        optimizer = OptimizerDdiff0(self.alpha,
                                    self.batch_size)
        self.D_diff_0 = optimizer.run(self.sigma0,self.t,self.x_meas,
                                      met_data, meas_data,
                                      self.maxiter)
        n_data, n_meas = meas_data.shape

        meas_data_diff = meas_data.copy()
        # Subtract Gaussian
        for i in range(n_data):
            c_adv = met_data[i]
            model = GaussianPlumeModel(c_adv, self.D_diff_0)
            model.solve(self.sigma0, self.t)
            for j in range(n_meas):
                meas_data_diff[i,j] -= model.evaluate_solution(self.x_meas[j])
        
        optimizer = OptimizerLinear(self.alpha,
                                    self.batch_size,
                                    self.basis)
        self.theta = optimizer.run(met_data,
                                   meas_data_diff,
                                   self.maxiter)

    def evaluate(self,k,v,subtract_gaussian=False):
        '''Evaluate at the k-th measurement point and for a particular
        advection velocity

        :arg k: index of measurement station
        :arg v: advective velocity
        :arg subtract_gaussian: Subtract the analytical Gaussian solution? 
        '''
        if (subtract_gaussian):
            y = 0.0
        else:
            model = GaussianPlumeModel(v, self.D_diff_0)
            model.solve(self.sigma0, self.t)
            y = model.evaluate_solution(self.x_meas[k])
        for i in range(self.basis.n):
            y += self.theta[k,i]*self.basis.evaluate(i,v)
        return y
