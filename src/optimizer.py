import numpy as np
from model import GaussianPlumeModel
from matplotlib import pyplot as plt

class OptimizerDdiff0(object):
    def __init__(self,alpha,batch_size):
        '''Optimize average diffusion coefficient using batched stochastic
        gradient descent

        :arg alpha: Learning rate
        :arg batch_size: Size of batches
        '''
        self.alpha = alpha
        self.batch_size = batch_size
        
    def run(self,sigma_0,t,x_meas,met_data,meas_data,maxiter=1000):
        '''Run the optimizer

        :arg sigma_0: Width of initial solution
        :arg t: time of solution
        :arg x_meas: measurement locations, array of size n_meas
        :arg met_data: velocities, array of length n_data 
        :arg meas_data: measured concentrations, array of size n_data x n_meas
        :arg maxiter: maximal number of iterations 
        '''
        
        n_data, n_meas = meas_data.shape
        D_diff_0 = 1.0
        loss_list = []
        for k in range(maxiter):
            j_samples = np.random.choice(np.arange(n_data),
                                         size=self.batch_size)
            delta = 0.0
            loss = 0.0
            for j in j_samples:
                c_adv = met_data[j]
                model = GaussianPlumeModel(c_adv, D_diff_0)
                model.solve(sigma_0, t)
                for i in range(n_meas):
                    x = x_meas[i]
                    y = meas_data[j,i]
                    diff = (y-model.evaluate_solution(x))
                    delta += diff*model.evaluate_grad_D0(x)
                    loss += diff**2
            D_diff_0 += self.alpha*delta
            loss_list.append(loss)
        plt.clf()
        plt.plot(np.arange(maxiter),loss_list,
                 linewidth=2,
                 color='blue')
        ax = plt.gca()
        ax.set_xlabel('iteration')
        ax.set_ylabel('loss')
        plt.savefig('loss.pdf',bbox_inches='tight')
        return D_diff_0

class OptimizerLinear(object):
    def __init__(self,alpha,batch_size,basis):
        '''Optimize average diffusion coefficient using batched stochastic
        gradient descent

        :arg alpha: Learning rate
        :arg batch_size: Size of batches
        :arg basis: function basis
        :arg D_diff_0: Estimate for D_diff_0
        '''
        self.alpha = alpha
        self.batch_size = batch_size
        self.basis = basis
        
    def run(self,met_data,meas_data,maxiter=1000):
        '''Run the optimizer

        :arg met_data: velocities, array of length n_data 
        :arg meas_data: measured concentrations, array of size n_data x n_meas
        :arg maxiter: maximal number of iterations 
        '''
        n_data, n_meas = meas_data.shape
        theta = np.zeros((n_meas,self.basis.n))
        loss_list = []
        for k in range(maxiter):
            j_samples = np.random.choice(np.arange(n_data),
                                         size=self.batch_size)
            loss = 0.0
            for j in j_samples:
                delta = np.zeros((n_meas,self.basis.n))
                c_adv = met_data[j]
                for ell in range(n_meas):
                    diff = meas_data[j,ell]
                    for i in range(self.basis.n):
                        diff -= theta[ell,i]*self.basis.evaluate(i,c_adv)
                    loss += diff**2
                    for k in range(self.basis.n):
                        delta[ell,k] += diff*self.basis.evaluate(k,c_adv)
            theta[:,:] += self.alpha*delta[:,:]
            loss_list.append(loss)
        plt.clf()
        plt.plot(np.arange(maxiter),loss_list,
                 linewidth=2,
                 color='blue')
        ax = plt.gca()
        ax.set_xlabel('iteration')
        ax.set_ylabel('loss')
        plt.savefig('loss_linear.pdf',bbox_inches='tight')
        return theta
