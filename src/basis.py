import numpy as np

class GaussianBasis(object):
    def __init__(self,n,x_min,x_max,sigma):
        self.n = n
        self.x_min = x_min
        self.x_max = x_max
        self.sigma = sigma

    def evaluate(self,k,x):
        '''Evaluate k-th basis function

        :arg k: index of basis function to evaluate
        :arg x: position at which to evaluate the basis function
        '''
        x_0 = self.x_min + float(k)/float(self.n-1.0)*(self.x_max-self.x_min)
        return np.exp(-0.5*(x-x_0)**2/self.sigma**2)
    
