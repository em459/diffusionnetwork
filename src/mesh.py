import numpy as np

class Mesh(object):
    ''' Mesh object, encoding the domain size and number of grid cells
    
    :arg L: extent of domain
    :arg n: number of grid cells
    '''
    def __init__(self, L, n):
        self.L = L
        self.n = n
        self.h = L/(1.*n)
