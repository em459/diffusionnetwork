\documentclass[11pt]{article}
\title{Data-based solution of 1d-diffusion problem}
\date{\today}
\usepackage[margin=2cm]{geometry}
\usepackage{amssymb,amsmath}
\usepackage{graphicx}
\newcommand{\figdir}{./figures/}
\begin{document}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Consider the 1d advection-diffusion with spatially varying diffusion coefficient $D(x)$ and advection velocity $v$. $D(x)$ is assumed to be constant, but $v$ can vary in a certain range $[v_{\min},v_{\max}]$. Given $v$ and some initial condition $u_0(x)$, we are looking for the solution $u(x,t|v)$ which satisfies
\begin{equation}
  \begin{aligned}
    \partial_t u &= \partial_x\left(D(x)\partial_x u\right) + v\partial_x u,\\
    u(x,t=0|v) &= u_0(x) = \frac{1}{\sqrt{2\pi\sigma_0^2}}\exp\left[-\frac{x^2}{2\sigma_0^2}\right].
  \end{aligned}
  \label{eqn:advection_diffusion}
\end{equation}
In fact, here we only consider the task of predicting the solution $u(x_i,t|v)$ at a set of $n_{\text{meas}}$ measuring stations $x_i$, $i=1,\dots,n_{\text{meas}}$. Assume that we solve Eq. \eqref{eqn:advection_diffusion} in a periodic domain $x\in[-L/2,L/2]$, and apply periodic boundary conditions in space, $u(L/2,t|v)=u(-L/2,t|v)$. For constant $D=\overline{D}$ the exact solution of Eq. \eqref{eqn:advection_diffusion} on the entire real axis is a Gaussian of growing width
\begin{equation*}
  u_G(x,t|v,\overline{D}) = \frac{1}{\sqrt{2\pi(\sigma_0^2+2\overline{D}t)}}\exp\left[-\frac{(x-vt)^2}{2(\sigma_0^2+2\overline{D}t)}\right].
\end{equation*}
In the following we describe how to construct an approximate solution $\widetilde{u}_i(v)=\widetilde{u}(x_i,t|v)\approx u(x_i,t|v)$ at time $t$ and the positions $x_i$, given the measured velocity $v$ (the met data).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Function approximation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Make the following ansatz, i.e. parametrise the approximate solution with a mean diffusion coefficient $\overline{D}$ and a sum of $n_{\text{basis}}$ functions $\phi_k$ in velocity space
\begin{equation}
  \widetilde{u}_i(v) = \widetilde{u}_i(v|\overline{D},\theta) = u_G(x_i,t|v,\overline{D})+\sum_{k=1}^{n_{\text{basis}}} \theta_{ik}\phi_k(v).
\end{equation}
Further, generate some synthetic data $\{(v^{(j)},u_i^{(j)})\}$ for $j=1,\dots,n_{\text{samples}}$ such that
\begin{equation}
  u_i^{(j)} = u(x_i,t|v^{(j)}) + \xi_j,\qquad\text{with $\xi_j\sim \mathcal{N}(0,\Sigma_u)$}\;\text{i.i.d.}.\label{eqn:data}
\end{equation}
In other words, the data is the exact solution plus some Gaussian noise with standard deviation $\Sigma_u$ (which could depend on the measurement station, but for simplicity we assume that it is the same for all stations). The data set can be split into $n_{\text{train}}$ training samples and $n_{\text{test}}=n_{\text{samples}}-n_{\text{train}}$ validation samples.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Fitting of model parameters}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To fit the parameters $\Theta:=(\overline{D},\theta)$ given the training data, use the following loss function, defined for a batch $J=(j_1,j_2,\dots,j_B)$, $j_\ell\in [1,n_{\text{train}}]$ of $B$ samples:
\begin{equation*}
  L_J(\overline{D},\theta) = \frac{1}{2}\sum_{j\in J}\left(u_i^{(j)}-\widetilde{u}_i(v^{(j)}|\overline{D},\theta)\right)^2.
\end{equation*}
To find the parameters $\Theta$ which parametrise the approximate solution $\widetilde{u}_i$, proceed in two steps:
\begin{enumerate}
\item Set $\theta=0$ and find $\overline{D}$ via Stochastic Gradient Descent (SGD)
\item Given the $\overline{D}$ from the first step, find $\theta$ via SGD 
\end{enumerate}
\paragraph{Step 1: Finding $\boldsymbol{\overline{D}}$.}
With $\theta=0$ we have $\widetilde{u}_i(v|\overline{D},0)=u_G(x_i,t|v,\overline{\theta})$. Hence, the update rule for the first SGD iteration is
\begin{equation}
  \begin{aligned}
    \overline{D}^{(\ell+1)} &= \overline{D}^{(\ell)}-\alpha \frac{\partial L_{J_{\ell}}}{\partial \overline{D}}(\overline{D}^{(\ell)},0)\\
    &= \overline{D}^{(\ell)} + \alpha\sum_{j\in J_\ell} \sum_{i=1}^{n_{\text{meas}}} \left(u_i^{(j)}-u_G(x_i,t|v^{(j)},\overline{D}^{(\ell)})\right)\frac{\partial u_G}{\partial \overline{D}}(x_i,t|v^{(j)},\overline{D}^{(\ell)})
    \end{aligned}\label{SGD:D}
\end{equation}
where $J_\ell$ is the batch picked at the $\ell$-th SGD step. A straightforward calculation gives
\begin{equation*}
  \frac{\partial u_G}{\partial \overline{D}}(x,t|v,\overline{D}) =
  \left(\frac{(x-vt)^2}{\sigma_0^2+2\overline{D}t}-1\right)\frac{t}{\sigma_0^2+2\overline{D}t}u_G(x,t|v\overline{D}).
\end{equation*}
Let $\overline{D}^*$ the value of $\overline{D}$ at the end of the SGD in the $\overline{D}$ direction. This value will be used in the second step for finding the parameters $\theta$
\paragraph{Step 2: Finding $\boldsymbol{\theta}$.}
Similarly, the update for the second SGD is
\begin{equation}
  \begin{aligned}
    \theta_{mn}^{(\ell+1)} &= \theta_{mn}^{(\ell)} - \alpha \frac{\partial L_{J_\ell}}{\partial\theta_{mn}}(\overline{D}^*,\theta_{mn}^{(\ell)})\\
    &= \theta_{mn}^{(\ell)} + \alpha \sum_{j\in J_\ell} \left(u_m^{(j)}-\widetilde{u}_m(v^{(j)}|\overline{D}^*,\theta^{(\ell)})\right)\phi_n(v^{(j)}).
    \end{aligned}\label{SGD:theta}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Basis functions $\boldsymbol{\phi}$}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For the following numerical experiments we choose the basis functions as a set of Gaussians over the domain $[\hat{v}_{\min},\hat{v}_{\max}]$ with
\begin{xalignat*}{2}
\hat{v}_{\min} &= v_{\min}-\frac{1}{4}(v_{\max}-v_{\min}),&
\hat{v}_{\max} &= v_{\max}+\frac{1}{4}(v_{\max}-v_{\min})
\end{xalignat*}

\begin{equation*}
  \phi_k(v) = \exp\left[-\frac{(x-\xi_k)^2}{2\Lambda^2}\right],\qquad\text{for $\xi_k = \hat{v}_{\min}+\frac{k-1}{n_{\text{basis}}-1}\left(\hat{v}_{\max}-\hat{v}_{\min}\right)$},\quad k=1,\dots,n_{\text{basis}}
\end{equation*}
where the width is set to $\Lambda=(\hat{v}_{\max}-\hat{v}_{\min})/n_{\text{basis}}$. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical experiments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For the numerical experiments we use the following fixed parameters:
\begin{xalignat*}{5}
  L &= 16, & \sigma_0 &= 0.5,& t &= 1, & v_{\min}&=-6, & v_{\max} &= +6
\end{xalignat*}
The function $D(x)$ is obtained by summing the first $2\cdot 8+1=17$ Fourier modes ($9$ cosines + $8$ sines) with randomly chosen coefficients, such that $D(x)\in[D_0-\delta D,D_0+\delta_0]$, $D_0=4.0$, $\delta D=3.75$. The spatially varying $D(x)$ is shown in Fig. \ref{fig:diffusion} (top).

The locations $x_i$ of the $n_{\text{meas}}=6$ measurement stations are chose uniformly in the domain, see Fig. \ref{fig:diffusion} (bottom), which also shows the solution $u(x,t|v)$ and the Gaussian approximation $u_G(x,t|v,D_{\text{avg}})$ with $D_{\text{avg}}$ set to the average of $D(x)$ over the domain ($D_{\text{avg}}$ is the coefficient of the lowest Fourier coefficient in the expansion of $D(x)$). In all plots we use $c_{\text{adv}}$ to denote the advection velocity $v$. The random noise on the measurements is set to $\Sigma_u=0.005$.
\begin{figure}
  \begin{center}
    \includegraphics[width=0.75\linewidth]{\figdir/diffusion.pdf}
    \caption{Spatially varying diffusion coefficient $D(x)$ (top), exact $u$ and approximate Gaussian plume solution $u_G$ for a given wind speed (bottom).}
    \label{fig:diffusion}
  \end{center}
\end{figure}
Fig. \ref{fig:measurements} visualises the synthetic data defined in Eq. \eqref{eqn:data} at each of the six measurement stations. To find the ``exact'' solution $u(x_i,t|v^{(j)})$ for each sample, Eq. \eqref{eqn:advection_diffusion} is solved with a forward Euler integrator on a spatial grid with 128 points. 
\begin{figure}
  \begin{center}
    \includegraphics[width=0.75\linewidth]{\figdir/measurements.pdf}
    \caption{Measured values $u_i^{(j)}$ (including the addded Gaussian noise) for all measurement stations, which are distinguished by different colours.}
    \label{fig:measurements}
  \end{center}
\end{figure}
A learning rate of $\alpha=0.1$ was used for the SGD update in Eqs. \eqref{SGD:D} and \eqref{SGD:theta}. In both cases $1024$ iterations were carried out with a batch size of 16. As the loss functions in Figs. \ref{fig:loss_D} and \ref{fig:loss_theta} show, SGD appears to have converged (note also that - as expected -  the first value in Fig. \ref{fig:loss_D} is identical to the final value in Fig. \ref{fig:loss_theta}).
\begin{figure}
  \begin{center}
    \includegraphics[width=0.75\linewidth]{\figdir/loss.pdf}
    \caption{Loss function of SGD iteration to determine $\overline{D}.$}
    \label{fig:loss_D}
  \end{center}
\end{figure}
\begin{figure}
  \begin{center}
    \includegraphics[width=0.75\linewidth]{\figdir/loss_linear.pdf}
    \caption{Loss function of SGD iteration to determine $\theta.$}
    \label{fig:loss_theta}
  \end{center}
\end{figure}
Finally, Fig. \ref{fig:evaluation} compares the predicted values of $\widetilde{u}_i(v)$ for each of the 6 measurement stations to the test data. For this, we actually subtracted the contribution from the Gaussian, i.e. the plots show the function
\begin{equation}
\delta \widetilde{u}_i(v) = \widetilde{u}_i(v) - u_G(x_i,t|v,\overline{D}^*)
\label{eqn:delta_u}
\end{equation}
in green and the corresponding training data
\begin{equation}
\delta u_i^{(j)} = u(x_i,t|v^{(j)}) + \xi_j - u_G(x_i,t|v^{(j)},\overline{D}^*)
\label{eqn:delta_data}
\end{equation}
in blue. Test data is shown in red, however here we removed the term $\xi_j$ from Eq. \eqref{eqn:delta_data}, so basically this is the exact solution to Eq. \eqref{eqn:advection_diffusion} minus the Guassian contribution $u_G$. As comparing the green curves and red test data points in Fig. \ref{fig:evaluation} confirms, the prediction is very good.
\begin{figure}
  \begin{center}
    \includegraphics[width=1.0\linewidth]{\figdir/evaluation.pdf}
    \caption{Performance evaluation. For each of the six measurement stations $i=1,\dots,6$ the following quantities are shown as a function of the advection velocity $v=c_{\text{adv}}$: Fitted solution $\delta\widetilde{u}_i(v)$ defined in Eq. \eqref{eqn:delta_u}, training data as in Eq. \eqref{eqn:delta_data} in blue and test data (with errors removed) in red.}
    \label{fig:evaluation}
  \end{center}
\end{figure}
\end{document}
