Diffusion network
=================

Data-guided neural-network solution of the diffusion problem, using a Gaussian prior.